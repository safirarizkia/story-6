[![pipeline status](https://gitlab.com/safirarizkia/story-6/badges/master/pipeline.svg)](https://gitlab.com/safirarizkia/story-6/commits/master)

[![coverage report](https://gitlab.com/safirarizkia/story-6/badges/master/coverage.svg)](https://gitlab.com/safirarizkia/story-6/commits/master)

## Author
Safira Rizki Anartya
1806191521

## Link Heroku
https://tellmeyourstory-6.herokuapp.com
