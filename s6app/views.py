from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

def index(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            status = form.save(commit=False)
            status.save()
            return redirect('/')
    else:
        form = StatusForm()
    status = Status.objects.all().order_by('-datentime')
    return render (request, 'index.html', {'form' : form, 'status' : status})
