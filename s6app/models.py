from django.db import models

# Create your models here.
class Status(models.Model):
    status = models.CharField(max_length = 300)
    datentime = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.status