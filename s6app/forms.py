from django import forms
from .models import Status
from django.forms import widgets

class StatusForm(forms.ModelForm):
    
    class Meta:
        model = Status
        fields = ['status']
        widgets = {
            'status' : forms.TextInput(attrs= {'cols': 10, 'rows': 5})
        }
    
    def __init__(self, *args, **kwargs):
        super(StatusForm, self).__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class' : 'form-control',
                'placeholder' : "Tell me how's today"
            })
