from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse

from .views import index
from .models import Status
from .forms import StatusForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options


# Create your tests here.
class StorySixUnitTest(TestCase):

    def test_landing_is_true(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response, 'halo, apa kabar?', status_code=200, html=True)

    def test_landing_has_correct_views(self):
        target = resolve('/')
        self.assertEqual(target.func, index)

    def create_status(self):
        new_status = Status.objects.create(status = "trial")
        return new_status

    def test_check_status(self):
        createStatus = self.create_status()
        self.assertTrue(isinstance(createStatus, Status))
        self.assertTrue(createStatus.__str__(), createStatus.status)
        count_all_status = Status.objects.all().count()
        self.assertEqual(count_all_status, 1)

    def test_form(self):
        formData = {
            'status' : 'a status',
        }
        form = StatusForm(data = formData)
        self.assertTrue(form.is_valid())
        request = self.client.post('/', data = formData)
        self.assertEqual(request.status_code, 302)
        
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

class StorySixFunctionalTest(LiveServerTestCase):
    def setUp(self):
        # firefox
        super(StorySixFunctionalTest, self).setUp()
        firefox_options = Options()
        firefox_options.add_argument('--no-sandbox')
        firefox_options.add_argument('--headless')
        firefox_options.add_argument('--disable-gpu')
        self.browser = webdriver.Firefox(firefox_options = firefox_options)
    
    def tearDown(self):
        self.browser.quit()
        super(StorySixFunctionalTest, self).tearDown()

    def test_input_status(self):
        self.browser.get('http://127.0.0.1:8000')

        status = self.browser.find_element_by_id('id_status')
        submit = self.browser.find_element_by_name('submit')

        status.send_keys("Coba Coba")
        submit.send_keys(Keys.RETURN)


